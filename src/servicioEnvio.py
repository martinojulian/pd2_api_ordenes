import requests
from excepciones import NoGeneraEnvioException, ServicioEnvioException
from flask import current_app

class ServicioEnvio:

    @staticmethod
    def generarEnvio(ordenId, libros):
        resp = requests.post(f'{current_app.config["URL_ENVIOS"]}envios',json={"ordenId": ordenId, "libros": libros})
        codResp = resp.status_code
        respJson = resp.json()
        if codResp == 200:
            return respJson.get("envioId")
        elif codResp == 409:
            raise NoGeneraEnvioException(respJson.get("msg"))
        else:
            raise ServicioEnvioException(respJson.get("msg"))