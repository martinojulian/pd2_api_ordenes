from flask.json import jsonify
from app import db    
from entidades import *
from excepciones import RegistroNoEncontradoException
from servicioLibros import ServicioLibros
from servicioEnvio import ServicioEnvio

class OrdenRepositorio:
    def crear(self, ordenJson):
        #orden = {"libros":[{"libroId": 1, "cantidad":2},...]}
        orden = OrdenEntidad()
        libros = ordenJson.get("libros")
        for libroCantidadJson in libros:
            if ServicioLibros.existeLibro(libroCantidadJson.get("libroId")):
                libroCantidad = LibroCantidadRepositorio().crear(libroCantidadJson)
                orden.librosCantidad.append(libroCantidad)
        db.session.add(orden)
        db.session.flush()
        envioId = ServicioEnvio.generarEnvio(orden.id,libros)
        orden.envioId = envioId
        db.session.add(orden)
        db.session.commit()

    def obtenerPorId(self, id):
        orden = OrdenEntidad.query.get(id)
        if orden:
            return orden.json()
        else:
            raise RegistroNoEncontradoException(f"La orden con el id: {id}, no existe")

    def listar(self):
        ordenes = OrdenEntidad.query.all()
        return [orden.json() for orden in ordenes]

class LibroCantidadRepositorio:
    def crear(self, libroCantidad):
        libroCantidad = LibroCantidadEntidad(libroCantidad)
        db.session.add(libroCantidad)
        return libroCantidad