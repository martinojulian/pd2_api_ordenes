from . import BaseTestClass
from entidades import OrdenEntidad, LibroCantidadEntidad
from app import db
import json
from servicioLibros import ServicioLibros
from servicioEnvio import ServicioEnvio
from mock import patch

class TestOrden(BaseTestClass):

    @patch.object(ServicioEnvio, 'generarEnvio')
    @patch.object(ServicioLibros, 'existeLibro')
    def testRutaCrearOrden(self, mockExisteLibro, mockGenerarOrden):
        mockExisteLibro.return_value = True
        mockGenerarOrden.return_value = 2
        with self.app.app_context():
            ordenCrear = {"libros": [{"libroId": 1, "cantidad":2}, {"libroId": 2, "cantidad":4}] }

            res = self.client.post('/ordenes',data=json.dumps(ordenCrear),content_type='application/json')
            
            assert res.status_code == 201
            data = json.loads(res.data.decode())
            assert data.get("msg") == "Orden generada"
            ordenCargada = OrdenEntidad.query.get(1)

            assert ordenCargada.envioId == 2
            librosCantidades = ordenCargada.librosCantidad
            assert len(librosCantidades) == 2
            libroCantidad = librosCantidades[0]  
            assert libroCantidad.libroId == ordenCrear.get("libros")[0].get("libroId")
            assert libroCantidad.cantidad == ordenCrear.get("libros")[0].get("cantidad")
            libroCantidad = librosCantidades[1]  
            assert libroCantidad.libroId == ordenCrear.get("libros")[1].get("libroId")
            assert libroCantidad.cantidad == ordenCrear.get("libros")[1].get("cantidad")

    def testRutaObtenerOrden(self):
        with self.app.app_context():
            orden = OrdenEntidad()
            libroCantidad1 = LibroCantidadEntidad({"libroId": 2, "cantidad":2})
            libroCantidad2 = LibroCantidadEntidad({"libroId": 1, "cantidad":4})
            orden.librosCantidad.append(libroCantidad1)
            orden.librosCantidad.append(libroCantidad2)
            db.session.add(libroCantidad1)
            db.session.add(libroCantidad2)
            db.session.add(orden)
            db.session.commit()

            res = self.client.get('/orden/1')
            
            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("id") == 1        
            assert data.get("envioId") == None        
            libros = data.get("libros")  
            assert len(libros) == 2          
            assert libros[0].get("libroId") == 2
            assert libros[0].get("cantidad") == 2
            assert libros[1].get("libroId") == 1
            assert libros[1].get("cantidad") == 4

    def testRutaObtenerOrdenNoExiste(self):
        with self.app.app_context():
            orden = OrdenEntidad()
            libroCantidad1 = LibroCantidadEntidad({"libroId": 2, "cantidad":2})
            libroCantidad2 = LibroCantidadEntidad({"libroId": 1, "cantidad":4})
            orden.librosCantidad.append(libroCantidad1)
            orden.librosCantidad.append(libroCantidad2)
            db.session.add(libroCantidad1)
            db.session.add(libroCantidad2)
            db.session.add(orden)
            db.session.commit()

            res = self.client.get('/orden/2')
            
            assert res.status_code == 404
            data = json.loads(res.data.decode())
            assert data.get("msg") == "La orden con el id: 2, no existe"  
            
    def testRutaListar(self):
        with self.app.app_context():
            orden = OrdenEntidad()
            libroCantidad1 = LibroCantidadEntidad({"libroId": 2, "cantidad":2})
            libroCantidad2 = LibroCantidadEntidad({"libroId": 1, "cantidad":4})
            orden.librosCantidad.append(libroCantidad1)
            orden.librosCantidad.append(libroCantidad2)
            db.session.add(libroCantidad1)
            db.session.add(libroCantidad2)
            db.session.add(orden)

            orden2 = OrdenEntidad({"envioId":2})
            libroCantidad3 = LibroCantidadEntidad({"libroId": 2, "cantidad":4})
            orden2.librosCantidad.append(libroCantidad3)
            db.session.add(libroCantidad3)
            db.session.add(orden2)
            db.session.commit()

            res = self.client.get('/ordenes')

            assert res.status_code == 200
            ordenes = json.loads(res.data.decode())
            assert len(ordenes) == 2
            orden = ordenes[0]
            assert orden.get("envioId") == None
            assert len(orden.get("libros")) == 2
            assert orden.get("libros")[0].get("libroId") == 2
            assert orden.get("libros")[0].get("cantidad") == 2
            assert orden.get("libros")[1].get("libroId") == 1
            assert orden.get("libros")[1].get("cantidad") == 4
            orden = ordenes[1]
            assert orden.get("envioId") == 2
            assert len(orden.get("libros")) == 1
            assert orden.get("libros")[0].get("libroId") == 2
            assert orden.get("libros")[0].get("cantidad") == 4