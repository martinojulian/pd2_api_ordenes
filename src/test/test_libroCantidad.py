from . import BaseTestClass
from entidades import LibroCantidadEntidad
from app import db
import json
from repositorio import LibroCantidadRepositorio

class TestLibroCantidad(BaseTestClass):

    def testCrearLibroCantidad(self):
        with self.app.app_context():
            libroCantidad = {"libroId": 1, "cantidad":2}
            
            libroCantidadCreado = LibroCantidadRepositorio().crear(libroCantidad)

            libroCantidad = LibroCantidadEntidad.query.get(1)
            assert libroCantidadCreado == libroCantidad
            assert libroCantidadCreado.libroId == libroCantidad.libroId
            assert libroCantidadCreado.cantidad == libroCantidad.cantidad

    def testLibroCantidadJson(self):
        with self.app.app_context():
            libroCantidadJson = {"libroId": 1, "cantidad":2}
            libroCantidad = LibroCantidadEntidad(libroCantidadJson)

            libroCantidadPasadoJoson = libroCantidad.json() 

            assert libroCantidadJson == libroCantidadPasadoJoson