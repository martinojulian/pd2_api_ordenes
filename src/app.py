from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()

def create_app(config="Config"):
    from rutas import ruta
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object("config.{}".format(config))  
    app.register_blueprint(ruta)
    db.init_app(app) 
    return app    