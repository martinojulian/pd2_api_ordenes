from app import db
from sqlalchemy.orm import relationship

class LibroCantidadEntidad(db.Model):
    __tablename__ = "libroCantidad" 
    id = db.Column(db.Integer,primary_key=True)
    libroId = db.Column(db.Integer)
    cantidad = db.Column(db.Integer)
    ordenId = db.Column(db.Integer, db.ForeignKey("orden.id"))
    orden = relationship("OrdenEntidad", back_populates="librosCantidad")

    def __init__(self, libroCantidad):
        self.libroId = libroCantidad.get('libroId')
        self.cantidad = libroCantidad.get('cantidad')

    def json(self):
        return {
            'libroId': self.libroId,
            'cantidad': self.cantidad,
        }

class OrdenEntidad(db.Model):
    __tablename__ = "orden" 
    id = db.Column(db.Integer,primary_key=True)
    envioId = db.Column(db.Integer)
    librosCantidad = relationship("LibroCantidadEntidad", back_populates="orden")

    def __init__(self, orden={}):
        self.envioId = orden.get('envioId')

    def json(self):
        return {
            'id': self.id,
            'envioId': self.envioId,
            'libros': [libro.json() for libro in self.librosCantidad]
        }