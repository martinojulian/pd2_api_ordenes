class RegistroNoEncontradoException(Exception):
    pass

class NoExisteLibroException(Exception):
    pass

class ServicioLibrosException(Exception):
    pass

class ServicioEnvioException(Exception):
    pass

class NoGeneraEnvioException(Exception):
    pass