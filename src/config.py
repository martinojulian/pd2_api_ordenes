from os import environ

class Config:
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    FLASK_ENV = environ.get("FLASK_ENV")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    URL_LIBROS = environ.get("URL_LIBROS")
    URL_ENVIOS = environ.get("URL_ENVIOS")

class ConfigTest(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI_TEST")
    DEBUG = True
