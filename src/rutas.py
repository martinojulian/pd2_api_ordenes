from flask import request,Blueprint, jsonify
from repositorio import *
from excepciones import NoGeneraEnvioException, NoExisteLibroException, ServicioLibrosException, ServicioEnvioException
import requests
ruta = Blueprint("rutas",__name__,url_prefix="")

@ruta.route('/orden/<id>')
def obtenerOrden(id):
    try:
        return OrdenRepositorio().obtenerPorId(id)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404

@ruta.route('/ordenes')
def listarOrdenes():
    return jsonify(OrdenRepositorio().listar()), 200

@ruta.route('/ordenes', methods=['POST'])
def crearOrden():
    try:
        orden = request.get_json()
        OrdenRepositorio().crear(orden)
        return jsonify({"msg": "Orden generada"}), 201
    except (NoGeneraEnvioException, NoExisteLibroException) as e:
        return jsonify({"msg": str(e)}), 409
    except (ServicioLibrosException, ServicioEnvioException) as e:
        return jsonify({"msg": str(e)}), 500
    except requests.exceptions.ConnectionError as e:
        return jsonify({"msg": "No se puede conectar a servicios"}), 500