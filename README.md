<h1>Api ordenes libros</h1>

<h3> Entidades </h3>

<ul>
    <li>
        LibroCantidad
        <ol>
            <li>id</li>
            <li>libroId</li>
            <li>cantidad</li>
            <li>ordenId</li>
        </ol>
    </li>
    <li>
        Orden
        <ol>
            <li>id</li>
            <li>envioId</li>
        </ol>
    </li>
</ul>

<h3> Rutas </h3>

<ul>
    <li>
        Orden
        <ol>
            <li>GET /ordenes</li>
            <li>GET /orden/&lt;int:id&gt;</li>
            <li>POST /ordenes</li>
        </ol>
</ul>
