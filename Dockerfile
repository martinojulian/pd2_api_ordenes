FROM python:3.9

WORKDIR /code

COPY ./src /code/

RUN pip install -r requirements.txt
